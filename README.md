# Rust Explicit Endian Conversion Library

## Introduction

The Rust Explicit Endian Conversion Library, named "explicit-endian," is a lightweight, no_std Rust crate that simplifies data conversion between different endianness formats. It serves as a valuable tool for managing binary data on systems with varying endianness, ensuring consistent and compatible data handling across different platforms.

## Features

- Conversion between little-endian and big-endian formats for various data types.
- Compatibility with no_std environments, making it suitable for embedded systems and resource-constrained scenarios.
- Support for a wide range of common integer types (u16, u32, u64, u128, i16, i32, i64, i128, usize, isize) and floating-point types (f32, f64).
- Comprehensive test suite to guarantee reliable functionality.

## Usage

To integrate the "explicit-endian" crate into your Rust project, add it as a dependency in your `Cargo.toml` file:

```toml
[dependencies]
explicit-endian = "*"
```

After adding the dependency, you can import the crate and utilize its functionality within your code:

```rust
extern crate explicit_endian as ee;

use ee::{LittleEndian, BigEndian, Swappable};

fn main() {
    let value = 42u32;

    // Convert to little-endian
    let le_value: LittleEndian<u32> = value.into();

    // Convert to big-endian
    let be_value: BigEndian<u32> = value.into();

    // You can now work with le_value and be_value in their respective endianness formats.
}
```

## Supported Data Types

The library supports the following data types for conversion:

- `u16`, `u32`, `u64`, `u128`
- `i16`, `i32`, `i64`, `i128`
- `usize`, `isize`
- `f32`, `f64`

## API Documentation

For in-depth information about the available functions and types, please consult the [API documentation](https://docs.rs/explicit-endian).

## Examples

You can explore usage examples and additional test cases in the `tests` module of the library's source code.

## License

This library is distributed under the terms of the Mozilla Public License, v. 2.0. You can obtain a copy of the MPL 2.0 license [here](https://mozilla.org/MPL/2.0/).

## Contributing

Contributions to this library are warmly welcomed. If you encounter any issues or have suggestions for improvements, please do not hesitate to open an issue or submit a pull request on the [GitHub repository](https://github.com/yourusername/explicit-endian).

## Acknowledgments

The "explicit-endian" crate was thoughtfully crafted by Alberto Ruiz <aruiz@gnome.org>. We extend our gratitude to the Rust community for their continuous support and valuable contributions. I would also like to thank OpenAI for generating this README file straight from the source code using ChatGPT.
